const initialState = {
  transactions: [],
  error: null,
  loading: true
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case 'GET_TRANSACTIONS':
      return {
        ...state,
        loading: false,
        transactions: payload
      };

    case 'DELETE_TRANS':
      return {
        ...state,
        transactions: state.transactions.filter(trans => trans._id !== payload)
      };
    case 'ADD_TRANS':
      return {
        ...state,
        transactions: [...state.transactions, payload]
      };
    case 'TRANSACTION_ERROR':
      return {
        ...state,
        error: payload
      };
    default:
      return state;
  }
};
