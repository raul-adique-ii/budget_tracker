import { combineReducers } from 'redux';
import reducersTrans from './reducersTrans';

export default combineReducers({
  reducersTrans
});
