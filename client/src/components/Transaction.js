import React from 'react';
import { connect } from 'react-redux';
import { delTrans } from '../actions/delTrans';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { numberWithCommas } from '../utils/format';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    fontSize: 25,
    padding: 10
  },
  button: {
    color: '#ff3d00'
  }
}));

const Transaction = ({ trans, delTrans }) => {
  const classes = useStyles();
  const sign = trans.amount < 0 ? '-' : '+';

  return (
    <div className={classes.root}>
      <div>
        <ListItem>{trans.text}</ListItem>
      </div>
      <div>
        <ListItem>
          <Typography
            style={{ color: trans.amount < 0 ? 'red' : 'green' }}
            variant='h5'
          >
            {sign}₱{numberWithCommas(Math.abs(trans.amount))}
          </Typography>
        </ListItem>
      </div>
      <div>
        <Button className={classes.button} onClick={() => delTrans(trans._id)}>
          x
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  ...state
});

export default connect(mapStateToProps, { delTrans })(Transaction);
