import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';

import Transaction from './Transaction';
import { getTransactions } from '../actions/delTrans';

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: 10,
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  history: {
    marginLeft: 'auto',
    marginright: 'auto',
    width: '56%',
    padding: 20
  }
}));

const TransList = ({ reducersTrans, getTransactions }) => {
  const { transactions } = reducersTrans;

  useEffect(() => {
    getTransactions();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const classes = useStyles();

  return (
    <div>
      <Typography className={classes.history} variant='h3'>
        History
      </Typography>
      <Divider />
      <List>
        {transactions.map(trans => (
          <div>
            <Paper className={classes.root}>
              <Transaction key={trans.id} trans={trans} />
            </Paper>
          </div>
        ))}
      </List>
    </div>
  );
};

const mapStateToProps = state => ({
  ...state
});

export default connect(mapStateToProps, { getTransactions })(TransList);
