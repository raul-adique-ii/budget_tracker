import React from 'react';
import { connect } from 'react-redux';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import { numberWithCommas } from '../utils/format';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-evenly',
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto',
    border: `2px solid ${theme.palette.divider}`,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.background.paper
  },
  inc: {
    color: '#388e3c'
  },
  exp: {
    color: '#ff3d00'
  },
  incexp: {
    textAlign: 'center'
  },
  divexp: {
    textAlign: 'center'
  }
}));

const IncomeExpense = ({ reducersTrans }) => {
  const { transactions } = reducersTrans;

  const amounts = transactions.map(trans => trans.amount);

  const income = amounts
    .filter(item => item > 0)
    .reduce((acc, item) => (acc += item), 0)
    .toFixed(2);

  const expense = (
    amounts.filter(item => item < 0).reduce((acc, item) => (acc += item), 0) *
    -1
  ).toFixed(2);

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.incexp}>
        <Typography variant='h6'>Income</Typography>
        <Typography className={classes.inc} variant='h4'>
          ₱{numberWithCommas(income)}
        </Typography>
      </div>
      <Divider orientation='vertical' flexItem />
      <div className={classes.divexp}>
        <Typography variant='h6'>Expense</Typography>
        <Typography className={classes.exp} variant='h4'>
          ₱{numberWithCommas(expense)}
        </Typography>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  ...state
});

export default connect(mapStateToProps)(IncomeExpense);
