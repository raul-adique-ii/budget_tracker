import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addTrans } from '../actions/delTrans';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    margin: 'auto'
  },
  text: {
    textAlign: 'center',
    padding: 20
  },
  inputtrans: {
    width: '80%',
    marginLeft: '12%'
  },
  inputamount: {
    width: '80%',
    marginLeft: '12%'
  },
  button: {
    background: '#80cbc4',
    color: 'white',
    width: '80%',
    marginLeft: '12%',
    padding: '1%'
  }
}));

const AddTrans = ({ addTrans }) => {
  const [text, setText] = useState('');
  const [amount, setAmount] = useState('');
  const classes = useStyles();

  const onSubmit = e => {
    e.preventDefault();

    const newTransaction = {
      id: Math.floor(Math.random() * 100000000),
      text,
      amount: +amount
    };
    addTrans(newTransaction);

    setText('');
    setAmount('');
  };

  return (
    <div className={classes.root}>
      <Typography className={classes.text} variant='h4'>
        Add a new transaction
      </Typography>
      <Typography className={classes.text} variant='h5'>
        (Put - for expense. Ex: -500)
      </Typography>
      <form onSubmit={onSubmit}>
        <div>
          <TextField
            className={classes.inputtrans}
            label='Transaction'
            variant='outlined'
            value={text}
            onChange={e => setText(e.target.value)}
            autoComplete='off'
          />
        </div>
        <br />
        <div>
          <TextField
            className={classes.inputamount}
            label='Amount'
            variant='outlined'
            value={amount}
            onChange={e => setAmount(e.target.value)}
            name='amount'
            autoComplete='off'
          />
        </div>

        <br />
        <Button className={classes.button} onClick={onSubmit}>
          Add Transaction
        </Button>
      </form>
    </div>
  );
};

const mapStateToProps = state => ({
  ...state
});

export default connect(mapStateToProps, { addTrans })(AddTrans);
