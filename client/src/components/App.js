import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';

import Header from './Header';
import Balance from './Balance';
import TransList from './TransList';
import IncomeExpense from './IncomeExpense';
import AddTrans from './AddTrans';

const useStyles = makeStyles(theme => ({
  root: {
    background: 'linear-gradient(45deg, #ffccbc  30%, #ffccbc 90%)'
  },
  paper: {
    borderRadius: 20,
    backgroundColor: '#f4f4f4',
    paddingBottom: 30
  }
}));

function App() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Container maxWidth='md'>
        <Paper className={classes.paper}>
          <Header />
          <div>
            <Balance />
            <IncomeExpense />
            <TransList />
            <AddTrans />
          </div>
        </Paper>
      </Container>
    </div>
  );
}

export default App;
