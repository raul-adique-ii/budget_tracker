import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: 30
  },
  title: {
    flexGrow: 1,
    textAlign: 'center'
  },
  bar: {
    background: '#80cbc4',
    color: '#fff',
    padding: 20,
    borderRadius: 10,
    width: '110%',
    marginLeft: -48
  }
}));

const Header = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar className={classes.bar} position='static'>
        <Typography className={classes.title} variant='h3'>
          Expense Tracker
        </Typography>
      </AppBar>
    </div>
  );
};

export default Header;
