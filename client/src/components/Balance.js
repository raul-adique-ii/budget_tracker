import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { numberWithCommas } from '../utils/format';

const useStyles = makeStyles(theme => ({
  root: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '29%',
    paddingTop: 40,
    padding: 10
  },
  balance: {
    textAlign: 'center'
  }
}));

const Balance = ({ reducersTrans }) => {
  const { transactions } = reducersTrans;

  const amounts = transactions.map(trans => trans.amount);

  const total = amounts.reduce((acc, item) => (acc += item), 0).toFixed(2);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography className={classes.balance} variant='h4'>
        Your Balance
      </Typography>
      <Typography
        className={classes.balance}
        variant='h3'
        style={{ color: total <= 500 ? 'red' : 'green' }}
      >
        ₱{numberWithCommas(total)}
      </Typography>
    </div>
  );
};

const mapStateToProps = state => ({
  ...state
});

export default connect(mapStateToProps)(Balance);
