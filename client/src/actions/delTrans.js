import axios from 'axios';

export const delTrans = id => async dispatch => {
  try {
    await axios.delete(`/api/v1/transactions/${id}`);

    dispatch({
      type: 'DELETE_TRANS',
      payload: id
    });
  } catch (err) {
    dispatch({
      type: 'TRANSACTION_ERROR',
      payload: err.response.data.error
    });
  }
};

export const addTrans = trans => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.post('/api/v1/transactions', trans, config);

    dispatch({
      type: 'ADD_TRANS',
      payload: res.data.data
    });
  } catch (err) {
    dispatch({
      type: 'TRANSACTION_ERROR',
      payload: err.response.data.error
    });
  }
};

export const getTransactions = () => async dispatch => {
  try {
    const res = await axios.get('/api/v1/transactions');

    dispatch({
      type: 'GET_TRANSACTIONS',
      payload: res.data.data
    });
  } catch (err) {
    dispatch({
      type: 'TRANSACTION_ERROR',
      payload: err.response.data.error
    });
  }
};
